package com.example.uapv1100778.tp1app;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        Intent intent = getIntent();
        final String pays = intent.getStringExtra("pays");

        final String image = CountryList.getCountry(pays).getmImgFile();
        final Resources res = this.getResources();
        int resId = res.getIdentifier(image, "drawable", getPackageName());

        ImageView iv = (ImageView) findViewById(R.id.imageView);
        iv.setImageResource(resId);

        TextView tvPays = (TextView) findViewById(R.id.textView6);
        tvPays.setText(pays);

        final TextView tvCapital = (TextView) findViewById(R.id.editText);
        tvCapital.setText(CountryList.getCountry(pays).getmCapital());

        final TextView tvLanguage = (TextView) findViewById(R.id.editText2);
        tvLanguage.setText(CountryList.getCountry(pays).getmLanguage());

        final TextView tvMonnaie = (TextView) findViewById(R.id.editText3);
        tvMonnaie.setText(CountryList.getCountry(pays).getmCurrency());

        final TextView tvPop = (TextView) findViewById(R.id.editText4);
        tvPop.setText(Integer.toString(CountryList.getCountry(pays).getmPopulation()));

        final TextView tvArea = (TextView) findViewById(R.id.editText5);
        tvArea.setText(Integer.toString(CountryList.getCountry(pays).getmArea()));

        Button saveButton = findViewById(R.id.button);
        saveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CountryList.getCountry(pays).setmCapital(tvCapital.getText().toString());
                CountryList.getCountry(pays).setmLanguage(tvLanguage.getText().toString());
                CountryList.getCountry(pays).setmCurrency(tvMonnaie.getText().toString());
                CountryList.getCountry(pays).setmPopulation(Integer.parseInt(tvPop.getText().toString()));
                CountryList.getCountry(pays).setmArea(Integer.parseInt(tvArea.getText().toString()));
                finish();
            }
        });

    }
}
